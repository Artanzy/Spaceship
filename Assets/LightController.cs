﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

	public GameObject LightObject;
	public bool LightStatus;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI () {
		if (LightStatus) {
			if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100), "TurnOff")) {
				LightStatus = false;
				LightObject.GetComponent<Light> ().enabled = false;
			}
		}
		else {
			if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100), "TurnOn")) {
				LightStatus = true;
				LightObject.GetComponent<Light> ().enabled = true;
			}
		}
	}
}
